import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Categoria from './components/Categoria.vue'
import Articulo from './components/Articulo.vue'
import Rol from './components/GestionUsuarios/Rol.vue'
import Usuario from './components/GestionUsuarios/Usuario.vue'
import Usuario_responsable from './components/GestionUsuarios/Usuario_responsable.vue'
import Cliente from './components/Cliente.vue'
import Proveedor from './components/Proveedor.vue'
import Login from './components/Login.vue'
import Ingreso from './components/Ingreso.vue'

//////////////////////////// registro pamco////////////////////////////
import Actualizar_parada from './components/Pamco/RegistroPamco/Actualizar_parada.vue'
import Eliminar_parada from './components/Pamco/RegistroPamco/Eliminar_parada.vue'
import Mostrar_parada from './components/Pamco/RegistroPamco/Mostrar_parada.vue'
import Registrar_parada from './components/Pamco/RegistroPamco/Registrar_parada.vue'
import Homes_Pamco from './components/Pamco/pagePamco.vue'
import Categoria_pamco from './components/Pamco/Categoria.vue'
import pruebas from './components/Pamco/RegistroPamco/pruebas.vue'
import pruebas2 from './components/Pamco/RegistroPamco/pruebas2.vue'

///////////////////////////// categoria perdidas///////////////////////
import Actualizar_perdida from './components/Pamco/CategoriaPerdidas/ActualizarPerdida.vue'
import Crear_perdida from './components/Pamco/CategoriaPerdidas/CrearPerdida.vue'
import Eliminar_perdida from './components/Pamco/CategoriaPerdidas/EliminarPerdida.vue'
import Mostrar_perdida from './components/Pamco/CategoriaPerdidas/MostrarPerdida.vue'

import RegistroAnomalia from './components/wcm/1_N/RegistroAnomalia.vue'
import Tecnico from './components/wcm/1_N/RegistrosAnomalias/Tecnico.vue'
import TecnicoLista from './components/wcm/1_N/RegistrosAnomalias/TecnicoLista.vue'
import TecnicoListaPropia from './components/wcm/1_N/RegistrosAnomalias/TecnicoListaPropio.vue'
import TecnicoListaPropiaConfirmado from './components/wcm/1_N/RegistrosAnomalias/TecnicoListaEsperaconfirmacion.vue'
import SupervisorLista from './components/wcm/1_N/RegistrosAnomalias/SupervisorLista.vue'
import Tarjetas from './components/wcm/1_N/RegistrosAnomalias/ListaInicialTarjetas.vue'
import TarjetaRoja from './components/wcm/1_N/RegistrosAnomalias/ListRojas.vue'
import TarjetaRojaAtender from './components/wcm/1_N/RegistrosAnomalias/ListRojasAtender.vue'
import TarjetaRojaRecibir from './components/wcm/1_N/RegistrosAnomalias/ListRojasRecibir.vue'
import TarjetaRojaCerrar from './components/wcm/1_N/RegistrosAnomalias/ListRojasCerrar.vue'

import TarjetaAzul from './components/wcm/1_N/RegistrosAnomalias/AzulList.vue'
import TarjetaAzulAtender from './components/wcm/1_N/RegistrosAnomalias/AzulAtender.vue'
import TarjetaAzulRecibir from './components/wcm/1_N/RegistrosAnomalias/AzulRecibir.vue'
import TarjetaAzulCerrar from './components/wcm/1_N/RegistrosAnomalias/AzulCerrar.vue'



import TarjetaAmarillaList from './components/wcm/1_N/RegistrosAnomalias/AmarillaList.vue'
import TarjetaAmarillaAtender from './components/wcm/1_N/RegistrosAnomalias/AmarillaAtender.vue'
import TarjetaAmarillaRecibir from './components/wcm/1_N/RegistrosAnomalias/AmarillaRecibir.vue'
import TarjetaAmarillaCerrar from './components/wcm/1_N/RegistrosAnomalias/AmarillaCerrar.vue'

import TarjetaVerdeList from './components/wcm/1_N/RegistroShe/List.vue'
import TarjetaVerdeAtencion from './components/wcm/1_N/RegistroShe/Atender.vue'
import TarjetaVerdeRecibir from './components/wcm/1_N/RegistroShe/Recibir.vue'
import TarjetaVerdeCerrar from './components/wcm/1_N/RegistroShe/Cerrar.vue'






import ProgrmacionTarjeta from './components/wcm/1_N/RegistrosAnomalias/ProgramacionTarjetas.vue'
import PanelTarjetas from './components/wcm/Dashboard/Panel.vue'
import PanelResumen from './views/wcm-tarjetas/wcm-reportes/wcm-dashboard.vue'

import Proyectos from './components/wcm/Proyecto.vue'


//Rutas de creacion de cruds simples

import Area from './components/wcm/Area.vue'
import Anomalia from './components/wcm/Anomalia.vue'
import CondicionInseg from './components/wcm/CondicionInsegura.vue'
import Suceso from './components/wcm/Suceso.vue'
import Falla from './components/wcm/Falla.vue'
import Maquina from './components/wcm/1_N/Maquina.vue'
import Sector from './components/wcm/1_N/Sector.vue'


////////////////////////////         datos meta  ////////////////////////////
/////////////// administrador, operador, lider, tecnico, ingenieria, operadorLider, supervisorProduccion, supervisorMantenimiendo, administradorWCM //////////////////



import store from './store'


Vue.use(Router)

var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/categorias',
      name: 'categorias',
      component: Categoria,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      },
      
    },
    {
      path: '/articulos',
      name: 'articulos',
      component: Articulo,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/roles',
      name: 'roles',
      component: Rol,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: Usuario,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/usuarios_responsables',
      name: 'usuarios_responsables',
      component: Usuario_responsable,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/ingresos',
      name: 'ingresos',
      component: Ingreso,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true
      }
    },
    {
      path: '/clientes',
      name: 'clientes',
      component: Cliente,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true
      }
    },
    {
      path: '/proveedores',
      name: 'proveedores',
      component: Proveedor,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true
      }
    },
    {
      path: '/registroanomalia',
      name: 'registroanomalia',
      component: RegistroAnomalia,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tecnico',
      name: 'tecnico',
      component: Tecnico,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tecnicoLista',
      name: 'tecnicoLista',
      component: TecnicoLista,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tecnicoListaPropia',
      name: 'tecnicoListaPropia',
      component: TecnicoListaPropia,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tecnicoListaPropiaConfirmado',
      name: 'tecnicoListaPropiaConfirmado',
      component: TecnicoListaPropiaConfirmado,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/supervisorLista',
      name: 'supervisorLista',
      component: SupervisorLista,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    
    {
      path: '/tarjetas',
      name: 'tarjetas',
      component: Tarjetas,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetaroja',
      name: 'tarjetaroja',
      component: TarjetaRoja,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetarojaatender',
      name: 'tarjetarojaatender',
      component: TarjetaRojaAtender,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetarojarecibir',
      name: 'tarjetarojarecibir',
      component: TarjetaRojaRecibir,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetarojacerrar',
      name: 'tarjetarojacerrar',
      component: TarjetaRojaCerrar,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetazul',
      name: 'tarjetazul',
      component: TarjetaAzul,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetazulatender',
      name: 'tarjetazulatender',
      component: TarjetaAzulAtender,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetazulrecibir',
      name: 'tarjetazulrecibir',
      component: TarjetaAzulRecibir,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetazulcerrar',
      name: 'tarjetazulcerrar',
      component: TarjetaAzulCerrar,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    
    {
      path: '/tarjeamarillalist',
      name: 'tarjeamarillalist',
      component: TarjetaAmarillaList,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetamarillatender',
      name: 'tarjetamarillatender',
      component: TarjetaAmarillaAtender,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetamarillarecibir',
      name: 'tarjetamarillarecibir',
      component: TarjetaAmarillaRecibir,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetamarillacerrar',
      name: 'tarjetamarillacerrar',
      component: TarjetaAmarillaCerrar,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetaverdelist',
      name: 'tarjetaverdelist',
      component: TarjetaVerdeList,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetaverdeatender',
      name: 'tarjetaverdeatender',
      component: TarjetaVerdeAtencion,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetaverderecibir',
      name: 'tarjetaverderecibir',
      component: TarjetaVerdeRecibir,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tarjetaverdecerrar',
      name: 'tarjetaverdecerrar',
      component: TarjetaVerdeCerrar,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    
    
    {
      path: '/busqueda',
      name: 'busqueda',
      component: ProgrmacionTarjeta,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/tablero',
      name: 'tablero',
      component: PanelTarjetas,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/resumen',
      name: 'resumen',
      component: PanelResumen,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/proyectos',
      name: 'proyectos',
      component: Proyectos,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }
    },
    {
      path: '/area',
      name: 'area',
      component: Area,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/anomalia',
      name: 'anomalia',
      component: Anomalia,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/condicion',
      name: 'condicion',
      component: CondicionInseg,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/suceso',
      name: 'suceso',
      component: Suceso,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/falla',
      name: 'falla',
      component: Falla,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/maquina',
      name: 'maquina',
      component: Maquina,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    {
      path: '/sector',
      name: 'sector',
      component: Sector,
      meta :{
        administrador :true,
        administradorWCM :true
      }
    },
    
    ////////////////////////////////////////////////registro pamco////////////////////////////////////
    {
      path: '/Registrar_parada',
      name: 'Registrar_parada',
      component: Registrar_parada,
      meta :{
        administrador :true
      }
    },
    {
      path: '/Actualizar_parada',
      name: 'Actualizar_parada',
      component: Actualizar_parada,
      meta :{
        administrador :true
      }
    },
    {
      path: '/Eliminar_parada',
      name: 'Eliminar_parada',
      component: Eliminar_parada,
      meta :{
        administrador :true
      }
    },
    {
      path: '/Mostrar_parada',
      name: 'Mostrar_parada',
      component: Mostrar_parada,
      meta :{
        administrador :true
      }
    },
    {
      path: '/Homes_Pamco',
      name: 'Homes_Pamco',
      component: Homes_Pamco,
      meta :{
        administrador :true,
        operador: true,
        lider :true, 
        tecnico :true, 
        ingenieria :true, 
        operadorLider :true, 
        supervisorProduccion :true, 
        supervisorMantenimiendo :true, 
        administradorWCM :true,
        responsableTarjetas: true
      }   
    },
    {
      path: '/Categoria_pamco',
      name: 'Categoria_pamco',
      component: Categoria_pamco,
      meta :{
        administrador :true
      }   
    },
    ////////////////////////////////////////////////fin registro pamco////////////////////////////////////
    ////////////////////////////////////////////////categoria perdidas ///////////////////////////////////
    {
      path: '/Actualizar_perdida',
      name: 'Actualizar_perdida',
      component: Actualizar_perdida,
      meta :{
        administrador :true
      }
    },
    {
      path: '/Crear_perdida',
      name: 'Crear_perdida',
      component: Crear_perdida,
      meta :{
        administrador :true
      }
    },
    {
      path: '/Eliminar_perdida',
      name: 'Eliminar_perdida',
      component: Eliminar_perdida,
      meta :{
        administrador :true
      }   
    },
    {
      path: '/Mostrar_perdida',
      name: 'Mostrar_perdida',
      component: Mostrar_perdida,
      meta :{
        administrador :true
      }   
    },
    {
      path: '/pruebas',
      name: 'pruebas',
      component: pruebas,
      meta :{
        administrador :true
      }   
    },
    {
      path: '/pruebas2',
      name: 'pruebas2',
      component: pruebas2,
      meta :{
        administrador :true
      }   
    },
    /////////////////////////////////////////////////////// fin categoria perdidas //////////////////////////////////////////// 
    
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta : {
        libre: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)){
    next()
  } else if (store.state.usuario && store.state.usuario.rol[0] == 'ADMINISTRADOR' || 
    store.state.usuario && store.state.usuario.rol[1] == 'ADMINISTRADOR' || 
    store.state.usuario && store.state.usuario.rol[2] == 'ADMINISTRADOR' ||
    store.state.usuario && store.state.usuario.rol[3] == 'ADMINISTRADOR' ||
    store.state.usuario && store.state.usuario.rol[4] == 'ADMINISTRADOR'){
    if (to.matched.some(record => record.meta.administrador)){
      next()
    }
    }else 
      if (store.state.usuario && store.state.usuario.rol[0]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[1]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[2]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[3]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[4]== 'OPERADOR')
      {
        if (to.matched.some(record => record.meta.operador))
        {
          next()
        }
      }
      else 
        if (store.state.usuario && store.state.usuario.rol[0]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[1]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[2]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[3]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[4]== 'LIDER')
        
        {
          if (to.matched.some(record => record.meta.lider))
          {
            next()
          }
        }else 
          if (store.state.usuario && store.state.usuario.rol[0]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[1]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[2]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[3]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[4]== 'TECNICO')
          {
            if (to.matched.some(record => record.meta.tecnico))
            {
              next()
            }
          }else 
            if (store.state.usuario && store.state.usuario.rol[0]== 'INGENIERIA' || 
            store.state.usuario && store.state.usuario.rol[1]== 'INGENIERIA' ||
            store.state.usuario && store.state.usuario.rol[2]== 'INGENIERIA' ||
            store.state.usuario && store.state.usuario.rol[3]== 'INGENIERIA' ||
            store.state.usuario && store.state.usuario.rol[4]== 'INGENIERIA')
            {
              if (to.matched.some(record => record.meta.ingenieria))
              {
                next()
    }
  }else if (store.state.usuario && store.state.usuario.rol[0]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[1]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[2]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[3]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[4]== 'OPERADOR LIDER'){

    if (to.matched.some(record => record.meta.operadorLider)){
      next()
    }
  }else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[1]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[2]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[3]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[4]== 'SUPERVISOR PRODUCCION'){
    if (to.matched.some(record => record.meta.supervisorProduccion)){
      next()
    }
  }else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[1]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[2]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[3]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[4]== 'SUPERVISOR MANTENIMIENTO'){
    if (to.matched.some(record => record.meta.supervisorMantenimiendo)){
      next()
    }
  }else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[1]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[2]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[3]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[4]== 'ADMINISTRADOR WCM'){
    if (to.matched.some(record => record.meta.administradorWCM)){
      next()
    }  
  } else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[1]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[2]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[3]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[4]== 'RESPONSABLE DE TARJETA'){
    if (to.matched.some(record => record.meta.responsableTarjetas)){
      next()
    }  
  }else{
    next({
      
      name: 'login'
    })
  }
})
/////////////// administrador, operador, lider, tecnico, ingenieria, operadorLider, supervisorProduccion, supervisorMantenimiendo, administradorWCM //////////////////

export default router