//Inicio de servidor
import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VImageInput from 'vuetify-image-input'
import PictureInput from 'vue-picture-input'
import './plugins/element.js'
import Vuesax from 'vuesax'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI, { size: 'small', zIndex: 3000 });

import 'vuesax/dist/vuesax.css'
Vue.use(Vuesax)
import WebCam from "vue-web-cam"
Vue.use(WebCam)
import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)
//import  '@/componentes/_globals'

import '@/components/_globals'


Vue.component(VImageInput.name, VImageInput,PictureInput.name,PictureInput,Vuesax,ElementUI);

Vue.config.productionTip = false
//axios.defaults.baseURL='https://190.181.38.50:8000'
//axios.defaults.baseURL='http://190.181.38.50:7000'
axios.defaults.baseURL='https://apiunileverservices.net/'  


//axios.defaults.baseURL='http://192.168.1.4:32774'  

//axios.defaults.baseURL='https://192.168.1.232:8001'  
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
